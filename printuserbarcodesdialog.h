#ifndef PRINTUSERBARCODESDIALOG_H
#define PRINTUSERBARCODESDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QtPrintSupport/QPrinter>
#include <QTableWidget>

#include <memory>

#include "userdb.h"


namespace Ui
{
class PrintUserBarcodesDialog;
}

class PrintUserBarcodesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PrintUserBarcodesDialog(QWidget *parent, std::shared_ptr<std::map<std::string, std::string>> &config, const std::shared_ptr<userDB> &userdb, const std::string &password);
    ~PrintUserBarcodesDialog();

private slots:
    void on_fontPushButton_clicked();

    void on_printerPushButton_clicked();

    bool eventFilter(QObject *obj, QEvent *event);

    void on_FilterLineEdit_editingFinished();

    void on_printSelectedUsersPushButton_clicked();

    void on_selectAllPushButton_clicked();

    void on_pushButton_clicked();

    void on_logoLineEdit_textChanged(const QString &arg1);

private:
    Ui::PrintUserBarcodesDialog *ui;
    std::shared_ptr<userDB> userdb;
    std::string password;
    std::list<QTableWidgetItem*> tableItemList;
    QFont selectedFont;
    QPrinter printer;
    std::shared_ptr<std::map<std::string, std::string>> config;
};

#endif // PRINTUSERBARCODESDIALOG_H
