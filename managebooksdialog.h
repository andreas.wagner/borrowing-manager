#ifndef MANAGEBOOKSDIALOG_H
#define MANAGEBOOKSDIALOG_H

#include <QWidget>
#include <QDialog>
#include <memory>
#include "bookcache.h"



namespace Ui
{
class ManageBooksDialog;
}

class ManageBooksDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ManageBooksDialog(std::shared_ptr<BookCache> book_cache, const std::shared_ptr<std::map<std::string, std::string>> &conf, QWidget *parent = nullptr, const std::string &isbn = "");
    ~ManageBooksDialog();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void on_ISBNLineEdit_editingFinished();

    void on_applyPushButton_clicked();

private:
    Ui::ManageBooksDialog *ui;
    std::shared_ptr<BookCache> book_cache;
    std::shared_ptr<std::map<std::string, std::string>> config;
};

#endif // MANAGEBOOKSDIALOG_H
