#ifndef PREPAREITEMSLIST_H
#define PREPAREITEMSLIST_H

#include <string>
#include <map>
#include <list>
#include <memory>

#include "userdb.h"

struct item
{
    enum type
    {
        INVALID,
        BORROWING,
        USER
    } type;
    union
    {
        Borrowing *borrowing;
        User *user;
    };
};

class prepareItemsList
{
public:
    enum type
    {
        SEARCH_QUERY,
        SEARCH_BARCODE,
        SEARCH_BORROWINGS
    };
    prepareItemsList(const type kind_of_query, const std::string &query, const std::shared_ptr<userDB> &userdb, const std::string &password);
    prepareItemsList(const prepareItemsList &&i);
    item getItem(int i) const;
    size_t getNumberOfItems() const;

private:
    void prepare_via_users(const std::shared_ptr<userDB> &userdb);
    void prepare_via_borrowings(const std::shared_ptr<userDB> &userdb);
    std::map<int, item> itemList;
    std::list<User> userList;
    std::list<Borrowing> borrowingList;
};

#endif // PREPAREITEMSLIST_H
