#ifndef CREATEBOOKDB_H
#define CREATEBOOKDB_H

#include <QWidget>
#include <QDialog>

namespace Ui
{
class CreateBookDBDialog;
}

class CreateBookDBDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateBookDBDialog(QWidget *parent = 0);
    ~CreateBookDBDialog();

private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

private:
    Ui::CreateBookDBDialog *dialog;
};

#endif // CREATEBOOKDB_H
