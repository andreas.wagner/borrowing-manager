#-------------------------------------------------
#
# Project created by QtCreator 2019-07-01T13:47:12
#
#-------------------------------------------------

QT       += core gui printsupport svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Borrowing_Manager_v2
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    createBookDB.cpp \
    createborrowerdbdialog.cpp \
    managebooksdialog.cpp \
    printuserbarcodesdialog.cpp \
    bookcache.cpp \
    dbconnection.cpp \
    urlfetch.cpp \
    userdb.cpp \
    recodestring.cpp \
    utc_time.cpp \
    prepareitemslist.cpp \
    getactiondescription.cpp \
    lookupbook.cpp \
    importusers_impl.cpp

HEADERS  += mainwindow.h \
    createBookDB.h \
    createborrowerdbdialog.h \
    managebooksdialog.h \
    printuserbarcodesdialog.h \
    bookcache.h \
    dbconnection.h \
    urlfetch.h \
    userdb.h \
    recodestring.h \
    utc_time.h \
    prepareitemslist.h \
    getactiondescription.h \
    lookupbook.h \
    importusers_impl.h

FORMS    += mainwindow.ui \
    printUserBarcodes.ui \
    createBookDB.ui \
    createBorrowerDB.ui \
    manageBooksDialog.ui \
    importusersdialog.ui


QMAKE_CXXFLAGS += -std=c++17

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += sqlite3


unix|win32: LIBS += -lrecode -lstdc++fs -lcurl
