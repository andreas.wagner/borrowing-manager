#ifndef GETACTIONDESCRIPTION_H
#define GETACTIONDESCRIPTION_H

#include <string>

#include "userdb.h"
#include "bookcache.h"

namespace getActionDescription
{
std::string borrowBook(const User &user, const Book &book);
std::string returnBook(const Borrowing &b, const BookCache &book_cache, const userDB &userdb, const std::string &password, int borrowingduration, int extraPeriodDuration);
std::string incrementExtraPeriods(const bool add_1_to_extraPeriods, const Borrowing &b, const BookCache &book_cache, const userDB &userdb, const std::string &password, int borrowingduration, int extraPeriodDuration);
}

#endif // GETACTIONDESCRIPTION_H
