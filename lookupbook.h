#ifndef LOOKUPBOOK_H
#define LOOKUPBOOK_H

#include "bookcache.h"
#include <string>

namespace lookupBook
{
    Book lookupBook(const std::string &isbn, const std::string &token);
};

#endif // LOOKUPBOOK_H
