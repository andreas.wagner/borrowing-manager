#include "importusers_impl.h"
#include "ui_importusersdialog.h"

#include <QFileDialog>
#include <fstream>

#include "recodestring.h"

importUsers_Impl::importUsers_Impl(QWidget *parent, const std::shared_ptr<userDB> &userdb, const std::string &password, const std::string &encoding):
    QDialog(parent),
    ui(new Ui::importUsersDialog),
    userdb(userdb),
    password(password),
    encoding(encoding)
{
    ui->setupUi(this);
}

void importUsers_Impl::on_pushButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Alle Dateien (*);;CSV-Datei (*.csv)"));
    std::string filename;
    if(dialog.exec())
    {
        std::string password = this->password;
        QStringList list = dialog.selectedFiles();
        for(QString item : list)
        {
            std::string filename = item.toUtf8().constData();
            std::ifstream input;
            std::string line;
            std::string tag;
            std::list<std::string> tag_list;
            size_t last_dot_pos = filename.find_last_of('.');
            size_t last_slash_pos = filename.find_last_of('/');

            // get tags from plaintextedit
            std::string tagString = ui->plainTextEdit->toPlainText().toUtf8().constData();
            size_t iterator = 0;
            size_t old_iterator = 0;
            while(iterator != std::string::npos)
            {
                old_iterator = iterator;
                iterator = tagString.find('\n', iterator);
                std::string toAdd = tagString.substr(old_iterator, iterator-old_iterator);
                if(!toAdd.empty())
                    tag_list.push_back(toAdd);
                if(iterator != std::string::npos)
                    iterator++;
            }

            if(ui->checkBox->checkState() == Qt::Checked)
            // FIXME: enable ~/data.sqlite/borrowers
            {
                if(last_slash_pos != std::string::npos)
                {
                    if(last_dot_pos != std::string::npos)
                    {
                        tag = filename.substr(last_slash_pos+1, last_dot_pos-last_slash_pos-1);
                    }
                    else
                    {
                        tag = filename.substr(last_slash_pos+1);
                    }
                }
                else
                {
                    if(last_dot_pos != std::string::npos)
                    {
                        tag = filename.substr(0, last_dot_pos);
                    }
                    else
                    {
                        tag = filename;
                    }
                }
                tag_list.push_back(tag);
            }
            input.open(filename);
            while(std::getline(input, line))
            {
                User u;
                line = recodeString::toUtf8(encoding, line);
                size_t delimiter_pos = line.find(';');
                u.firstname = line.substr(0, delimiter_pos);
                u.lastname = line.substr(delimiter_pos + 1);
                if(u.lastname.find('\r') != std::string::npos)
                {
                    u.lastname = u.lastname.substr(0, u.lastname.find('\r'));
                }
                std::string id = userdb->addUser(u, password);
                u = userdb->getUserByBarcode(id, password);
                userdb->setTagsForUser(tag_list, u);
            }
            input.close();
        }
    }
}
