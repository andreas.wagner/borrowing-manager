#include "managebooksdialog.h"
#include "ui_manageBooksDialog.h"
#include "bookcache.h"
#include "lookupbook.h"
#include <QString>
#include <QMessageBox>
#include <string>
#include <memory>
#include <QEvent>
#include <QKeyEvent>

ManageBooksDialog::ManageBooksDialog(std::shared_ptr<BookCache> book_cache, const std::shared_ptr<std::map<std::string, std::string>> &conf, QWidget *parent, const std::string &isbn):
    QDialog(parent),
    ui(new Ui::ManageBooksDialog),
    book_cache(book_cache),
    config(conf)
{
    ui->setupUi(this);
    ui->ISBNLineEdit->installEventFilter(this);
    ui->ISBNLineEdit->setText(isbn.c_str());
}

ManageBooksDialog::~ManageBooksDialog()
{
    delete ui;
}

bool ManageBooksDialog::eventFilter(QObject *obj, QEvent *event)
{
    if(obj == ui->ISBNLineEdit && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if(Qt::Key_Enter == keyEvent->key() || Qt::Key_Return == keyEvent->key())
        {
            return true;
        }
    }
    return QObject::eventFilter(obj, event);
}

void ManageBooksDialog::on_ISBNLineEdit_editingFinished()
{
    std::string toFind = std::string(ui->ISBNLineEdit->text().toUtf8().constData());
    if(toFind.length() == 10 || toFind.length() == 13)
    {
        Book b = book_cache->getBookByISBN(toFind);
        if(b.title == "" && b.authors == "")
        {
            // TODO: Fehlerbehandlung
            Book tmp = lookupBook::lookupBook(toFind, (*config).at("AccessToken"));
            if(tmp.ISBN != "")
            {
                book_cache->updateBook(tmp);
                ui->AuthorLineEdit->setText(tmp.authors.c_str());
                ui->TitleLineEdit->setText(tmp.title.c_str());
                book_cache->updateBook(tmp);
            }
            else
            {
                ui->AuthorLineEdit->setText("");
                ui->TitleLineEdit->setText("");
            }
        }
        else {
            ui->AuthorLineEdit->setText(b.authors.c_str());
            ui->TitleLineEdit->setText(b.title.c_str());
        }
    }
    else
    {
        ui->AuthorLineEdit->setText("");
        ui->TitleLineEdit->setText("");
    }
}


void ManageBooksDialog::on_applyPushButton_clicked()
{
    std::string toFind = std::string(ui->ISBNLineEdit->text().toUtf8().constData());
    if(toFind.length() == 10 || toFind.length() == 13)
    {
        Book b;
        b.ISBN = toFind;
        b.authors = ui->AuthorLineEdit->text().toUtf8().constData();
        b.title = ui->TitleLineEdit->text().toUtf8().constData();
        book_cache->updateBook(b);
    }
    ui->ISBNLineEdit->setText("");
    ui->AuthorLineEdit->setText("");
    ui->TitleLineEdit->setText("");
}
