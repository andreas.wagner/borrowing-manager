#include "prepareitemslist.h"
#include "userdb.h"

void prepareItemsList::prepare_via_users(const std::shared_ptr<userDB> &userdb)
{
    int i = 0;
    for(auto usr_iter = userList.begin(); usr_iter != userList.end(); usr_iter++)
    {
        std::list<Borrowing> borrowings_part(std::move(userdb->getBorrowingsForUser(*usr_iter)));
        item listItem;
        listItem.type = item::USER;
        listItem.user = &(*usr_iter);
        itemList[i] = listItem;
        i++;
        for(auto borr_iter = borrowings_part.begin(); borr_iter != borrowings_part.end(); borr_iter++)
        {
            borrowingList.push_back(*borr_iter);
            item listItem;
            listItem.type = item::BORROWING;
            listItem.borrowing = &(borrowingList.back());
            itemList[i] = listItem;
            i++;
        }
    }
}

void prepareItemsList::prepare_via_borrowings(const std::shared_ptr<userDB> &userdb)
{
    int i = 0;
    for(auto borr_iter = borrowingList.begin(); borr_iter != borrowingList.end(); borr_iter++)
    {
        item listItem;
        listItem.type = item::BORROWING;
        listItem.borrowing = &(*borr_iter);
        itemList[i] = listItem;
        i++;
    }
}

prepareItemsList::prepareItemsList(const type kind_of_query, const std::string &query, const std::shared_ptr<userDB> &userdb, const std::string &password)
{
    if(kind_of_query == type::SEARCH_QUERY)
    {
        userList = userdb->getUsersForSearchstring(password, query);
        prepare_via_users(userdb);
    }
    else if(kind_of_query == type::SEARCH_BARCODE)
    {
        userList.push_back(userdb->getUserByBarcode(query, password));
        prepare_via_users(userdb);
    }
    else if(kind_of_query == type::SEARCH_BORROWINGS)
    {
        borrowingList = userdb->getBorrowingsForISBN(query);
        prepare_via_borrowings(userdb);
    }

}

prepareItemsList::prepareItemsList(const prepareItemsList &&i):
    itemList(std::move(i.itemList)),
    userList(std::move(i.userList)),
    borrowingList(std::move(i.borrowingList))
{
}

item prepareItemsList::getItem(int i) const
{
    return itemList.at(i);
}

size_t prepareItemsList::getNumberOfItems() const
{
    return itemList.size();
}
