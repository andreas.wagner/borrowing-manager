#ifndef URLFETCH_H
#define URLFETCH_H

#include <string>

namespace URLFetch
{
    std::string fetchHTTPToString(const std::string &url);
};

#endif // URLFETCH_H
