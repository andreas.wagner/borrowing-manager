#include "createBookDB.h"
#include "ui_createBookDB.h"
#include "bookcache.h"
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>

CreateBookDBDialog::CreateBookDBDialog(QWidget *parent) :
    QDialog(parent),
    dialog(new Ui::CreateBookDBDialog)
{
    dialog->setupUi(this);
}

CreateBookDBDialog::~CreateBookDBDialog()
{
    delete dialog;
}

void CreateBookDBDialog::on_buttonBox_accepted()
{
    BookCache bookCache;
    QString filename = dialog->lineEdit->text();
    try
    {
        QFile newFile(filename);
        newFile.open(QIODevice::ReadWrite); //create file
        newFile.close();
        bookCache.open(filename.toUtf8().constData());
    }
    catch (std::runtime_error &e)
    {
        QMessageBox msg;
        msg.setText((std::string("CreateBookDBDialog: Öffnen der Datenbank schlug fehl. ")+ e.what()).c_str());
        msg.exec();
    }
    catch (std::exception &e)
    {
        QMessageBox msg;
        msg.setText(e.what());
        msg.exec();
    }

    try
    {
        bookCache.initializeDatabase();
    }
    catch (std::runtime_error &e)
    {
        QMessageBox msg;
        msg.setText((std::string("CreateBookDBDialog: Initialisieren der Datenbank schlug fehl.")+e.what()).c_str());
        msg.exec();
    }
    catch (std::exception &e)
    {
        QMessageBox msg;
        msg.setText(e.what());
        msg.exec();
    }
    bookCache.close();
}

void CreateBookDBDialog::on_pushButton_clicked()
{
    QFileDialog fileDialog(this, "Bücher-Datenbank erstellen unter", "", "SQLite-Datei (*.sqlite);;Alle Dateien (*)");
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setFileMode(QFileDialog::AnyFile);
    int result = fileDialog.exec();
    if(result)
    {
        QStringList list = fileDialog.selectedFiles();
        dialog->lineEdit->setText(list[0]);
    }
}
