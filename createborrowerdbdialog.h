#ifndef CREATEBORROWERDBDIALOG_H
#define CREATEBORROWERDBDIALOG_H

#include <QWidget>
#include <QDialog>

namespace Ui
{
class CreateBorrowerDBDialog;
}

class CreateBorrowerDBDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateBorrowerDBDialog(QWidget *parent = 0);
    ~CreateBorrowerDBDialog();

private slots:
    void on_hidePasswordCheckBox_stateChanged(int arg1);

    void on_DBPasswordLineEdit_editingFinished();

    void on_buttonBox_accepted();

    void on_SelectDBFilePushButton_clicked();

private:
    Ui::CreateBorrowerDBDialog *ui;
};

#endif // CREATEBORROWERDBDIALOG_H
