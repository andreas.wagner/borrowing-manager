<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB" sourcelanguage="de_DE">
<context>
    <name>CreateBookDBDialog</name>
    <message>
        <location filename="createBookDB.ui" line="14"/>
        <source>Bücher-Datenbankdatei anlegen</source>
        <translation>Create Book-Database</translation>
    </message>
    <message>
        <location filename="createBookDB.ui" line="22"/>
        <source>Dateiname</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location filename="createBookDB.ui" line="29"/>
        <source>Auswählen</source>
        <translation>Choose</translation>
    </message>
</context>
<context>
    <name>CreateBorrowerDBDialog</name>
    <message>
        <location filename="createBorrowerDB.ui" line="14"/>
        <source>Entleiher-Datenbank anlegen</source>
        <translation>Create Borrower-Database</translation>
    </message>
    <message>
        <location filename="createBorrowerDB.ui" line="22"/>
        <source>Dateiname</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location filename="createBorrowerDB.ui" line="32"/>
        <source>Auswählen</source>
        <translation>Choose</translation>
    </message>
    <message>
        <location filename="createBorrowerDB.ui" line="39"/>
        <source>Passwort</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="createBorrowerDB.ui" line="49"/>
        <source>verstecken</source>
        <translation>hide</translation>
    </message>
    <message>
        <location filename="createBorrowerDB.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sie können mit einem Klick auf „verstecken“ das Passwort ein- und ausblenden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By clicking „hide“ you c an hide the password.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="createborrowerdbdialog.cpp" line="43"/>
        <source>Passwort leer</source>
        <translation>No Password Set</translation>
    </message>
    <message>
        <location filename="createborrowerdbdialog.cpp" line="43"/>
        <source>Die Entleiher-Datenbank muss mit einem Passwort verschlüsset werden.
Keine Datei angelegt.</source>
        <translation>The borrower-database has to be created with a apssword.\nNo file created.</translation>
    </message>
    <message>
        <location filename="createborrowerdbdialog.cpp" line="60"/>
        <source>Neue Entleiher-Datenbankdatei</source>
        <translation>New Borrower Databasefile</translation>
    </message>
    <message>
        <location filename="createborrowerdbdialog.cpp" line="62"/>
        <source>SQLite-Datei (*.sqlite)</source>
        <translation>SQLite-File(*.sqlite)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Ausleih-Manager v2</source>
        <translation>Borrowing Manager v2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Entleiher</source>
        <translation>Borrower</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>Benutzer</source>
        <translation>Users</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Vorname</source>
        <translation>First Name</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <source>Nachname</source>
        <translation>Last Name</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <source>Barcode</source>
        <translation>Barcode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="229"/>
        <source>Tags, ein Tag je Zeile</source>
        <translation>Tags, one tag per line</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="267"/>
        <source>Übernehmen</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="274"/>
        <location filename="mainwindow.cpp" line="52"/>
        <source>Abbrechen</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Entliehene Bücher</source>
        <translation>Borrowed Books</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <source>Inventar editieren</source>
        <translation>Edit Inventory</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="318"/>
        <source>Aktualisieren</source>
        <translation>Reload</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="326"/>
        <source>Administration</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <location filename="mainwindow.ui" line="358"/>
        <source>Anlegen</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Access-Token der DNB</source>
        <translatorcomment>&quot;DNB&quot; is the german national library</translatorcomment>
        <translation>Access Token of the DNB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <location filename="mainwindow.ui" line="399"/>
        <source>Auswählen</source>
        <translation>Choose</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="385"/>
        <source>Bücher-Datenbankdatei</source>
        <translation>Book-Databasefile</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="392"/>
        <source>verstecken</source>
        <translation>hide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="413"/>
        <source>Entleiher-Datenbankdatei</source>
        <translation>Borrower-Databasefile</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <source>Passwort</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <location filename="mainwindow.ui" line="434"/>
        <location filename="mainwindow.ui" line="441"/>
        <source>☐</source>
        <translation>☐</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="494"/>
        <source>Verlängerung jeweils (Stunden)</source>
        <translation>Extra Periods (Hours)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="504"/>
        <source>Ausleihdauer (Stunden)</source>
        <translation>Borrowing-Time (Hours)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="539"/>
        <location filename="mainwindow.ui" line="547"/>
        <source>Entleiher importieren</source>
        <translation>Import Borrowers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Entleiher importieren und mit Dateiname taggen</source>
        <translation>Import Borrowers and Tag with Filename</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <source>Encoding</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="601"/>
        <source>Benutzer auswählen um Barcodes auszudrucken</source>
        <translation>Select Users to Print Barcodes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <source>Bereits geliehen!</source>
        <translation>Already Borrowed!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <source>Der Kunde hat das Buch bereits geliehen.</source>
        <translation>The customer already borrowed this book.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <source>Weiteres Exemplar leihen</source>
        <translation>Borrow another specimen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="51"/>
        <location filename="mainwindow.cpp" line="685"/>
        <location filename="mainwindow.cpp" line="849"/>
        <source>Buch zurückgeben</source>
        <translation>Return Book</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>Alle Dateien (*);;CSV-Datei (*.csv)</source>
        <translation>All Files (*);;CSV-Files (*.csv)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>Entleiher-Datenbank auswählen</source>
        <translation>Choose Borrower Databasefile</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <location filename="mainwindow.cpp" line="565"/>
        <source>SQLite-Datei (*.sqlite);; Alle Dateien (*)</source>
        <translation>SQLite-File (*.sqlite);;All Files (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="686"/>
        <location filename="mainwindow.cpp" line="850"/>
        <source>Ausleihe verlängern</source>
        <translation>Get Extra Period</translation>
    </message>
</context>
<context>
    <name>ManageBooksDialog</name>
    <message>
        <location filename="manageBooksDialog.ui" line="14"/>
        <source>Inventar organisieren</source>
        <translation>Organize Inventory</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nachdem eine ISBN eingegeben wurde, wird das Buch in der Datenbank oder online gesucht. Ein Klick auf  &lt;span style=&quot; background-color:transparent;&quot;&gt;„Übernehmen“ speichert die Angaben in der Datenbank ab.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;After entering an ISBN, the computer will search tha database or online for the corresponding information. Clicking „Apply“ saves the information to the database.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="32"/>
        <source>Autor</source>
        <translation>Author</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="39"/>
        <location filename="manageBooksDialog.ui" line="46"/>
        <source>Wird später geladen...</source>
        <translation>To be Loaded...</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="53"/>
        <source>Titel</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="60"/>
        <source>ISBN</source>
        <translation>SIBN</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="73"/>
        <source>Bitte eingeben!</source>
        <translation>Please enter!</translation>
    </message>
    <message>
        <location filename="manageBooksDialog.ui" line="82"/>
        <source>Übernehmen</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>PrintUserBarcodesDialog</name>
    <message>
        <location filename="printUserBarcodes.ui" line="14"/>
        <source>Barcodes drucken</source>
        <translation>Print Barcodes</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="29"/>
        <source>Alle auswählen</source>
        <translation>Select All</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="36"/>
        <source>Suchen</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="59"/>
        <source>Schriftart auswählen!</source>
        <translation>Select a font!</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="72"/>
        <source>Schriftart</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="85"/>
        <source>Drucker</source>
        <translation>Printer</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="92"/>
        <source>Bitte Drucker auswählen!</source>
        <translation>Please select a printer!</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="115"/>
        <source>Logo auswählen (.svg)</source>
        <translation>Select Logo (.svg)</translation>
    </message>
    <message>
        <location filename="printUserBarcodes.ui" line="127"/>
        <source>Ausgewählte Benutzer drucken</source>
        <translation>Print Selected Users</translation>
    </message>
</context>
</TS>
