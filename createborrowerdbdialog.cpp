#include "createborrowerdbdialog.h"
#include "ui_createBorrowerDB.h"
#include "userdb.h"
#include <QFileDialog>
#include <QMessageBox>

CreateBorrowerDBDialog::CreateBorrowerDBDialog(QWidget *parent):
    QDialog(parent),
    ui(new Ui::CreateBorrowerDBDialog)
{
    ui->setupUi(this);
}

CreateBorrowerDBDialog::~CreateBorrowerDBDialog()
{
    delete ui;
}

void CreateBorrowerDBDialog::on_hidePasswordCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Unchecked)
    {
        ui->DBPasswordLineEdit->setEchoMode(QLineEdit::EchoMode::Normal);
    }
    else
    {
        ui->DBPasswordLineEdit->setEchoMode(QLineEdit::EchoMode::Password);
    }
}

void CreateBorrowerDBDialog::on_DBPasswordLineEdit_editingFinished()
{
    ui->hidePasswordCheckBox->setCheckState(Qt::CheckState::Checked);
}

void CreateBorrowerDBDialog::on_buttonBox_accepted()
{
    userDB userdb;
    std::string filename(ui->DBFilenameLineEdit->text().toUtf8().constData());
    std::string password(ui->DBPasswordLineEdit->text().toUtf8().constData());
    if(password.empty())
    {
        QMessageBox::information(this, tr("Passwort leer"), tr("Die Entleiher-Datenbank muss mit einem Passwort verschlüsset werden.\nKeine Datei angelegt."));
    }
    if(!filename.empty() && !password.empty())
    {
        if(QFile::exists(filename.c_str()))
        {
            QFile::remove(filename.c_str());
        }
        userdb.open(filename);
        userdb.initializeDatabase(password);
    }
}

void CreateBorrowerDBDialog::on_SelectDBFilePushButton_clicked()
{
    QFileDialog fileDialog(this, "Neue Entleiher-Datenbankdatei", "", "SQLite-Datei (*.sqlite);;Alle Dateien (*)");
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setFileMode(QFileDialog::AnyFile);
    int result = fileDialog.exec();
    if(result)
    {
        QStringList list = fileDialog.selectedFiles();
        ui->DBFilenameLineEdit->setText(list[0]);
    }
}
