#include "recodestring.h"

#ifndef _my_RECODE_H_
#define _my_RECODE_H_
#include <recode.h>
#endif

#include <iostream>

void recodeString::initialize()
{
    outer = recode_new_outer(true);
}

void recodeString::finalize()
{
    recode_delete_outer(outer);
}

std::string recodeString::toUtf8(const std::string &encoding, const std::string &str)
{
    char * c_str = (char *)str.c_str();
    size_t size1 = str.size() *4;
    size_t size2 = size1;
    char * ret = new char[size1]();
    RECODE_REQUEST request = recode_new_request(outer);
    recode_scan_request(request, std::string(encoding + "..UTF-8").c_str());
    recode_string_to_buffer(request, c_str, &ret, &size1, &size2);
    recode_delete_request(request);
    std::string returnString(ret);
    delete[] ret;
    return returnString;
}
