#ifndef IMPORTUSERS_IMPL_H
#define IMPORTUSERS_IMPL_H

#include <QWidget>
#include <QDialog>

#include <memory>

#include "userdb.h"

namespace Ui
{
    class importUsersDialog;
}

class importUsers_Impl : public QDialog
{
    Q_OBJECT
public:
    importUsers_Impl(QWidget *parent, const std::shared_ptr<userDB> &userdb, const std::string &password, const std::string &encoding);

private slots:
    void on_pushButton_clicked();

private:
    Ui::importUsersDialog *ui;
    std::shared_ptr<userDB> userdb;
    std::string password;
    std::string encoding;
};

#endif // IMPORTUSERS_IMPL_H
